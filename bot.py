#!/usr/bin/env python

# Imports from standardlib
import time
import json
import subprocess
import sys
from datetime import datetime, timedelta



# Install and load selenium components & webdriver
try:
	from webdriver_manager.chrome import ChromeDriverManager
except ImportError:
	subprocess.check_call([sys.executable, "-m", "pip", "install", 'webdriver-manager'])
finally:
	from webdriver_manager.chrome import ChromeDriverManager

try:
	from selenium.webdriver.common.by import By
	from selenium.webdriver.chrome.service import Service
	from selenium.webdriver.support.ui import Select
	from selenium.webdriver import ChromeOptions, Chrome
except ImportError:
	subprocess.check_call([sys.executable, "-m", "pip", "install", 'selenium'])
finally:
	from selenium.webdriver.common.by import By
	from selenium.webdriver.chrome.service import Service
	from selenium.webdriver.support.ui import Select
	from selenium.webdriver import ChromeOptions, Chrome

try:
	from apscheduler.schedulers.blocking import BlockingScheduler
except ImportError:
	subprocess.check_call([sys.executable, "-m", "pip", "install", 'apscheduler'])
finally:
	from apscheduler.schedulers.blocking import BlockingScheduler


# Initialize selenium
options = ChromeOptions()
options.add_experimental_option("detach", True)
service = Service(ChromeDriverManager().install())
driver = Chrome(chrome_options=options, service=service )

# Get dates
today = datetime.now()
today_str = today.strftime("%Y-%m-%d")
tomorrow = today + timedelta(days=1)
tomorrow_str = tomorrow.strftime("%Y-%m-%d")


def booking_job():

	# Load userdata
	with open('userdata.json', 'r') as f:        
		userdata = json.load(f)
		print(userdata)



	# Establish chrome driver and go to report site URL
	url = "https://buchung.hsz.rwth-aachen.de/angebote/Wintersemeseter_2021_22/_Turnen_Level_3.html"
	driver.get(url)

	# Find booking button on first page
	course_row = driver.find_element(By.XPATH, "//tr[@id='bs_tr08001CCCEE8E']")
	booking_cell = course_row.find_element(By.CLASS_NAME, 'bs_sbuch')
	

	while True:
		try:
			booking_link = booking_cell.find_element(By.TAG_NAME, 'input')
			break
		except:
			print("Noch nicht freigeschaltet")
			driver.refresh()
			time.sleep(1)

	#time.sleep(5)
	booking_link.click()

	# Switch to newly opend tab
	driver.switch_to.window(driver.window_handles[1])

	# Find link for upcoming course
	element = driver.find_element(By.NAME, "BS_Termin_"+tomorrow_str)
	element.click()

	# Fill out all fields
	element = driver.find_element(By.NAME, "vorname")
	element.send_keys(userdata["vorname"])

	element = driver.find_element(By.NAME, "name")
	element.send_keys(userdata["name"])

	element = driver.find_element(By.NAME, "strasse")
	element.send_keys(userdata["strasse"])

	element = driver.find_element(By.NAME, "ort")
	element.send_keys(userdata["ort"])

	element = driver.find_element(By.NAME, "email")
	element.send_keys(userdata["email"])

	element = driver.find_element(By.NAME, "telefon")
	element.send_keys(userdata["telefon"])



	select = Select(driver.find_element(By.NAME, "statusorig"))
	select.select_by_value(userdata["statusorig"])

	radio = driver.find_element_by_css_selector("input[type='radio'][value='M']").click()

	time.sleep(0.5)
	if userdata["statusorig"] == "S-RWTH":
		element = driver.find_element(By.NAME, "matnr")
		element.send_keys(userdata["matnr"])

	if userdata["statusorig"] == "B-RWTH":
		element = driver.find_element(By.NAME, "mitnr")
		element.send_keys(userdata["mitnr"])

	time.sleep(0.5)
	element = driver.find_element(By.NAME, "iban")
	element.send_keys(userdata["iban"])

	element = driver.find_element(By.NAME, "tnbed")
	element.click()

	elements = driver.find_elements_by_css_selector("input[class='sub']")
	time.sleep(5)
	elements[3].click()


	## CAUTION: If this step is executed, the course will be booked at your cost
	elements = driver.find_elements_by_css_selector("input[class='sub']")
	elements[1].click()

	time.sleep(5)


if __name__=='__main__':
	scheduler = BlockingScheduler(timezone="Europe/Berlin")
	scheduler.add_job(booking_job, 'date', run_date=today_str+' 20:15:00')
	scheduler.start()


