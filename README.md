# hszBot

## Anleitung
1. git repository clonen
2. userdata.json ausfüllen
	- sex: M oder W
	- satusorig: S-RWTH für Studierende, B-RWTH für Beschäftigte
		- Wichtig: Auf Groß-/Kleinschreibung achten
	- matnr oder mitnr (Diensttelefon) je nach Status ausfüllen
	- Der Rest sollte selbsterklärend sein. Bei Telefonnummer und IBAN keine Leerzeichen einfügen
	- Beispiel ist in userdata_muster.json
3. bot_test.py mit python3 ausführen, je nach system über "python3 bot_test.py" oder "python bot_test.py"
	- Bei dem Testskript wird für einen Beispielkurs (Softball) getestet, ob alles so weit funktioniert. Der Kurs wird aber am Ende natürlich nicht gebucht.
	- Wenn das Browserfenster am Ende die letzte Buchungsseite vor "kostenpflichtig Buchen anzeigt, klappt alles.

4. Danach könnt ihr die bot.py ausführen, dabei wird fürs aktuelle Datum ein Buchungsjob für den Turnen-Kurs am nächsten Tag angelegt, der um 20:15 ausgeführt wird. 
	- Wichtig: Der Computer muss bis dahin natürlich laufen, also darf nicht in den Ruhestand gehen oder ausgeschaltet werden.
5. Keine Garantie, dass es klappt :D


